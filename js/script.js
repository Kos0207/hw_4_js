let number1
while (number1 !== 0 && (!number1 || isNaN(number1))) {
    number1 = +prompt("Введіть перше число")
}
let number2
while (number2 !== 0 && (!number2 || isNaN(number2))) {
    number2 = +prompt("Введіть друге число")
}
let operator
const operators = ["+", "-", "*", "/"]
while (!operators.includes(operator)) {
    operator = prompt("Виберіть арифметичну дію із наведених: +, -, *, /")
}
function calc(a, b, c) {
    let result
    switch (c) {
        case "+":
            result = a + b
            break;
        case "-":
            result = a - b
            break;
        case "*":
            result = a * b
            break;
        case "/":
            result = a / b
            break;
    }
    console.log("Число1:", a, "Число2:", b, "Оператор", c, "Результат", result);
}
calc(number1, number2, operator)